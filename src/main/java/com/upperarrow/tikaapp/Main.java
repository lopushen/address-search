package com.upperarrow.tikaapp;

import com.upperarrow.tikaapp.fileprocessing.FileProvider;
import com.upperarrow.tikaapp.fileprocessing.ReportWriter;
import com.upperarrow.tikaapp.task.FileParser;
import com.upperarrow.tikaapp.utils.PropertiesHolder;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.ConcurrentLinkedQueue;

import static com.upperarrow.tikaapp.utils.PropertiesHolder.inputFolderName;
import static com.upperarrow.tikaapp.utils.PropertiesHolder.outputFolderName;


public final class Main {
    private static org.apache.log4j.Logger log = Logger.getLogger(Main.class);
    public static String fileName;


    public static void main(String... aArgs) {
        PropertiesHolder.init(aArgs);
        PropertyConfigurator.configure("log4j.properties");
        log.info("Processing folder " + inputFolderName + ", Report file dir: " + outputFolderName);
        // create the actual filename and delete the existing if exists
        fileName = outputFolderName + "/" + new SimpleDateFormat("yyyyMMdd").format(new Date());
        new File(fileName).delete();
        new File(outputFolderName).mkdirs();
        ReportWriter reportGenerator = ReportWriter.getInstance();
        reportGenerator.createFileIfNotExists();
        FileProvider fileProvider = new FileProvider();
        ConcurrentLinkedQueue<File> files = fileProvider.getFiles(inputFolderName);
        FileParser parser = new FileParser();
        parser.parseAll(files);
    }
} 