package com.upperarrow.tikaapp.domain;

public class AddressItem {

    /**
     * Country,City,AccentCity,Region,Population,Latitude,Longitude
     */

    private String country;
    private String city;
    private String accentCity;
    private String region;
    private String postalCode;
//    private String population;
//    private String latitude;
//    private String longtitude;

    public AddressItem(String country, String city, String accentCity, String region) {
        this.country = country;
        this.city = city;
        this.accentCity = accentCity;
        this.region = region;

    }

    public AddressItem(String country, String city, String accentCity, String region, String postalCode) {
        this.country = country;
        this.city = city;
        this.accentCity = accentCity;
        this.region = region;
        this.postalCode = postalCode;
    }

    public String getCountry() {
        return country;
    }

    public String getCity() {
        return city;
    }

    public String getAccentCity() {
        return accentCity;
    }

    public String getRegion() {
        return region;
    }

    public String getPostalCode() {
        return postalCode;
    }

    @Override
    public String toString() {
        return "com.upperarrow.tikaapp.domain.AddressItem{" +
                "country='" + country + '\'' +
                ", city='" + city + '\'' +
                ", accentCity='" + accentCity + '\'' +
                ", region='" + region + '\'' +
                '}';
    }
}
