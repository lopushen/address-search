package com.upperarrow.tikaapp.domain;

public class AdvancedAddressItem {
    private String countryCode;
    private String postalCode;
    private String placeName;


    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getPlaceName() {
        return placeName;
    }

    public void setPlaceName(String placeName) {
        this.placeName = placeName;
    }

    @Override
    public String toString() {
        return "com.upperarrow.tikaapp.domain.AdvancedAddressItem{" +
                "countryCode='" + countryCode + '\'' +
                ", postalCode='" + postalCode + '\'' +
                ", placeName='" + placeName + '\'' +
                '}';
    }
}
