package com.upperarrow.tikaapp.domain;

import java.io.File;

public class Row {
    private File file;
    private int phoneNumberCount;
    private int addressCount;
    private String containsDictionary;


    public Row(File file) {
        this.file = file;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public int getPhoneNumberCount() {
        return phoneNumberCount;
    }

    public void setPhoneNumberCount(int phoneNumberCount) {
        this.phoneNumberCount = phoneNumberCount;
    }

    public int getAddressCount() {
        return addressCount;
    }

    public void setAddressCount(int addressCount) {
        this.addressCount = addressCount;
    }

    public String getContainsDictionary() {
        return containsDictionary;
    }

    public void setContainsDictionary(String containsDictionary) {
        this.containsDictionary = containsDictionary;
    }
}