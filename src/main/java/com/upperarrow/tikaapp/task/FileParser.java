package com.upperarrow.tikaapp.task;

import com.upperarrow.tikaapp.tikautils.AddressText;
import org.apache.log4j.Logger;

import java.io.File;
import java.util.Queue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.IntStream;

import static com.upperarrow.tikaapp.utils.PropertiesHolder.numberOfThreads;


public class FileParser {
    private static org.apache.log4j.Logger log = Logger.getLogger(FileParser.class);

    public void parseAll(Queue<File> files) {
        // init the cache - temp fix
        AddressText.getINSTANCE();
        CountDownLatch latch = new CountDownLatch(numberOfThreads);
        ExecutorService executorService = Executors.newFixedThreadPool(numberOfThreads);
        IntStream.range(0, numberOfThreads).mapToObj(value -> new Thread(new ParsingTask(files, latch))).forEach(Thread::run);
        try {
            latch.await();
        } catch (InterruptedException e) {
            log.error("The parser execution has been interrupted " + e.getMessage());
        }
        executorService.shutdown();
    }
}
