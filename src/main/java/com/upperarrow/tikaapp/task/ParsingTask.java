package com.upperarrow.tikaapp.task;

import com.upperarrow.tikaapp.domain.Row;
import com.upperarrow.tikaapp.fileprocessing.ReportWriter;
import com.upperarrow.tikaapp.tikautils.AddressAndPhoneExtractionContentHandler;
import org.apache.log4j.Logger;
import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.sax.BodyContentHandler;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Queue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.stream.Collectors;

import static com.upperarrow.tikaapp.utils.PropertiesHolder.parsingTimeout;

public class ParsingTask implements Runnable {

    private Queue<File> files;
    private static org.apache.log4j.Logger log = Logger.getLogger(ParsingTask.class);
    private ExecutorService executorService = Executors.newFixedThreadPool(1);
    private CountDownLatch latch;

    public ParsingTask(Queue<File> files, CountDownLatch latch) {
        this.latch = latch;
        this.files = files;
    }

    @Override
    public void run() {
        File file = files.poll();
        while (file != null) {
            final File finalFile = file;
            Future<Row> future = executorService.submit(() -> parseSingleFile(finalFile));
            try {
                future.get(parsingTimeout, TimeUnit.MILLISECONDS);
            } catch (InterruptedException | ExecutionException e) {
                log.error("An error occurred when running the parsing task ", e);
            } catch (TimeoutException e) {
                log.info("The parsing of file " + file.getName() + " has been timeout, it took more than " + String.valueOf(parsingTimeout) + " milliseconds");
            }
            file = files.poll();
        }

        latch.countDown();
        executorService.shutdown();
    }

    private Row parseSingleFile(File file) {
        log.info("Started parsing file " + file.getName());
        AutoDetectParser parser = new AutoDetectParser();
        Metadata metadata = new Metadata();
        AddressAndPhoneExtractionContentHandler addressHandler = new AddressAndPhoneExtractionContentHandler(new BodyContentHandler(-1), metadata);
        try (InputStream stream = new FileInputStream(file)) {
            ParseContext context = new ParseContext();
            parser.parse(stream, addressHandler, metadata, context);
        } catch (SAXException | TikaException | IOException e) {
            log.error("Error parsing text " + e.getMessage());
        }
        String[] addresses = metadata.getValues("addresses");
        String[] phones = metadata.getValues("phonenumbers");
        String containsDictionary = metadata.get("containsPhoneAddress");
        Row row = null;
        if (addresses.length > 0 || phones.length > 0) {
            row = new Row(file);
            if (addresses.length > 0) {
                log.info("Addresses " + file.getName() + " " + Arrays.stream(addresses).collect(Collectors.joining(",")));
                row.setAddressCount(addresses.length);
            }
            if (phones.length > 0) {
                log.info("Phone numbers " + file.getName() + " " + Arrays.stream(phones).collect(Collectors.joining(",")));
                row.setPhoneNumberCount(phones.length);
            }
            row.setContainsDictionary(containsDictionary);
            ReportWriter.getInstance().writeToFile(row);
        }
        log.info("Ended parsing file " + file.getName());
        return row;
    }
}
