package com.upperarrow.tikaapp.tikautils;

import com.upperarrow.tikaapp.Main;
import com.upperarrow.tikaapp.utils.PropertiesHolder;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.sax.ContentHandlerDecorator;
import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AddressAndPhoneExtractionContentHandler extends ContentHandlerDecorator {
    private Metadata metadata;
    private static final String addresses = "addresses";
    private static final String PHONE_NUMBERS = "phonenumbers";
    private StringBuilder stringBuilder;


    /**
     * Creates a decorator for the given SAX event handler and Metadata object.
     *
     * @param handler SAX event handler to be decorated
     */
    public AddressAndPhoneExtractionContentHandler(ContentHandler handler, Metadata metadata) {
        super(handler);
        this.metadata = metadata;
        this.stringBuilder = new StringBuilder();
    }

    /**
     * Creates a decorator that by default forwards incoming SAX events to
     * a dummy content handler that simply ignores all the events. Subclasses
     * should use the {@link #setContentHandler(ContentHandler)} method to
     * switch to a more usable underlying content handler.
     * Also creates a dummy Metadata object to store phone numbers in.
     */
    protected AddressAndPhoneExtractionContentHandler() {
        this(new DefaultHandler(), new Metadata());
    }

    /**
     * The characters method is called whenever a Parser wants to pass raw...
     * characters to the ContentHandler. But, sometimes, phone numbers are split
     * accross different calls to characters, depending on the specific Parser
     * used. So, we simply add all characters to a StringBuilder and analyze it
     * once the document is finished.
     */
    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        try {
            String text = new String(Arrays.copyOfRange(ch, start, start + length));
            stringBuilder.append(text);
            super.characters(ch, start, length);
        } catch (SAXException e) {
            handleException(e);
        }
    }


    /**
     * This method is called whenever the Parser is done parsing the file. So,
     * we check the output for any phone numbers.
     */
    @Override
    public void endDocument() throws SAXException {
        super.endDocument();
        String text = stringBuilder.toString();
        List<String> addresses = AddressText.getINSTANCE().extractAdvancedAddresses(text);
        for (String address : addresses) {
            metadata.add(AddressAndPhoneExtractionContentHandler.addresses, address);
        }

        metadata.add("containsPhoneAddress",containsAddressDictionaryWord(text)?"1":"0");

        List<String> numbers = PhoneText.extractPhoneNumbers(text);
        for (String number : numbers) {
            metadata.add(PHONE_NUMBERS, number);
        }
    }

    private boolean containsAddressDictionaryWord(String text) {
        Pattern pattern = Pattern.compile(PropertiesHolder.phoneDictionary);
        Matcher matcher = pattern.matcher(text.toLowerCase());
        return matcher.find();
    }
}
