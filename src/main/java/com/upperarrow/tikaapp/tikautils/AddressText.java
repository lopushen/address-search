package com.upperarrow.tikaapp.tikautils;

import com.upperarrow.tikaapp.domain.AddressItem;
import com.upperarrow.tikaapp.domain.AdvancedAddressItem;
import com.upperarrow.tikaapp.Main;
import com.upperarrow.tikaapp.utils.PropertiesHolder;
import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public final class AddressText {
    private static org.apache.log4j.Logger log = Logger.getLogger(AddressText.class);
    public static final String ALL_COUNTRIES_TXT = "allCountries.txt";
    public static final String TWO_WORD_BOUNDARIES = "\\b%s\\b.{0,3}\\b%s\\b";
    static List<AddressItem> addressCache;
    private List<AdvancedAddressItem> advancedAddressCache;

    private static AddressText INSTANCE;

    public static AddressText getINSTANCE() {
        if (INSTANCE == null) {
            INSTANCE = new AddressText();
        }
        return INSTANCE;
    }

    private AddressText() {
        fillAdvancedAddressCache();
    }

    public ArrayList<String> extractAddressesByRegex(String text, String regex) {
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(text);
        ArrayList<String> addresses = new ArrayList<>();
        while (m.find()) {
            String address = m.group(0);
            addresses.add(address);
        }
        return addresses;
    }

    public List<String> extractAdvancedAddresses(String text) {
        if (text != null) {
            List<AdvancedAddressItem> items = collectContainedAddresses(text);
            return items.stream().map(AdvancedAddressItem::getPlaceName).collect(Collectors.toList());
        }
        return new ArrayList<>();
    }

    private void fillAdvancedAddressCache() {
        long time = System.currentTimeMillis();

        try (BufferedReader br = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream("/"+PropertiesHolder.locationsFileName)))) {
            // skip the header of the csv
            advancedAddressCache = br.lines().map(line -> {
                String[] rows = line.split("\t");
                AdvancedAddressItem advancedAddressItem = new AdvancedAddressItem();
                advancedAddressItem.setCountryCode(isNotNullToLoweCase(rows[0]));
                advancedAddressItem.setPostalCode(isNotNullToLoweCase(rows[1]));
                advancedAddressItem.setPlaceName(isNotNullToLoweCase(rows[2]));
                return advancedAddressItem;
            }).collect(Collectors.toList());

            advancedAddressCache = Collections.unmodifiableList(advancedAddressCache);
            log.info("Initialized advanced cache for " + String.valueOf(System.currentTimeMillis() - time));
        } catch (IOException e) {
            e.printStackTrace();
            System.err.println(e.getMessage());
        }
    }

    private String isNotNullToLoweCase(String text) {
        if (text != null) return text.toLowerCase().trim();
        else return "";
    }

    private boolean containsRegex(String text, String addressComponent1, String addressComponent2) {
        String loggableAddressComponent1 = addressComponent1;
        String loggableAddressComponent2 = addressComponent2;
        addressComponent1 = Pattern.quote(addressComponent1.toLowerCase());
        addressComponent2 = Pattern.quote(addressComponent2.toLowerCase());
        Pattern pattern = Pattern.compile(String.format(TWO_WORD_BOUNDARIES, addressComponent1, addressComponent2));
        Matcher matcher = pattern.matcher(text.toLowerCase());
        boolean b1 = matcher.find();
        if (b1) log.info("Match! Found " + loggableAddressComponent1 + " " + loggableAddressComponent2);
        return b1;
    }


    private List<AdvancedAddressItem> collectContainedAddresses(String text) {
        if (text == null || text.trim().equals("")) return new ArrayList<>();

        return advancedAddressCache.parallelStream().filter(
                address -> containsRegex(text, address.getPostalCode(), address.getPlaceName()) ||
                        containsRegex(text, address.getPlaceName(), address.getPostalCode())
        ).collect(Collectors.toList());
    }
}
