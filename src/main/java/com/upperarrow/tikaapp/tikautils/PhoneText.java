package com.upperarrow.tikaapp.tikautils;


import com.upperarrow.tikaapp.Main;
import com.upperarrow.tikaapp.utils.PropertiesHolder;

import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.ArrayList;

public class PhoneText {
    // Regex to identify a phone number
    static final String cleanPhoneRegex = "(%s)([ ;:-]{0,3})([2-9]\\d{2}[2-9]\\d{6})";

    // Regex which attempts to ignore punctuation and other distractions.
    static final String phoneRegex = "([{(<]{0,3}[2-9][\\W_]{0,3}\\d[\\W_]{0,3}\\d[\\W_]{0,6}[2-9][\\W_]{0,3}\\d[\\W_]{0,3}\\d[\\W_]{0,6}\\d[\\W_]{0,3}\\d[\\W_]{0,3}\\d[\\W_]{0,3}\\d)/i";

    public static ArrayList<String> extractPhoneNumbers(String text) {
        // Currently no cleaning
        //text = clean(text);
        int idx = 0;
        Pattern p = Pattern.compile(String.format(cleanPhoneRegex, PropertiesHolder.phoneDictionary), Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(text);
        ArrayList<String> phoneNumbers = new ArrayList<String>();
        while (m.find(idx)) {
            String digits = m.group(3);
            int start = m.start(3);
            int end = m.end(3);
            String prefix = "";
            if (start > 0) {
                prefix = text.substring(start - 1, start);
            }
            if (digits.substring(0, 2).equals("82") && prefix.equals("*")) {
                // this number overlaps with a *82 sequence
                idx += 2;
            } else {
                // seems good
                phoneNumbers.add(digits);
                idx = end;
            }
        }
        return phoneNumbers;
    }
}