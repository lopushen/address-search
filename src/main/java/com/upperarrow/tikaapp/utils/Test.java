package com.upperarrow.tikaapp.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Test {

    public static final String TEXT = "xjjxjxjxjxj\n" +
            "dshfgsdfkjgsadkgsdf\n" +
            "asdfasdlkfhksajdfhksadfh\n" +
            "sdjflksadjflksdjflksad\n" +
            "shflksdaflksdhfhsdkfhsdkjfhkjsdhfkjsdfh\n" +
            "\n" +
            "\n" +
            "fskdjhfkjkjshfkjasd\n" +
            "\n" +
            "\n" +
            "Canillo AD100\n" +
            "\n" +
            "AD200 d Encamp";
    public static final String TWO_WORD_BOUNDARIES = "\\b%s\\b.{0,4}\\b%s\\b";

    public static void main(String[] args) {
        System.out.println(containsRegex(TEXT, "AD200", "Encamp"));
    }


    private static boolean containsRegex(String text, String addressComponent1, String addressComponent2) {
//        s = String.format(BOUNDARIES, isNotNullToLoweCase(s));
        addressComponent1 = Pattern.quote(addressComponent1.toLowerCase());
        addressComponent2 = Pattern.quote(addressComponent2.toLowerCase());
        Pattern pattern = Pattern.compile(String.format(TWO_WORD_BOUNDARIES, addressComponent1, addressComponent2));
        Matcher matcher = pattern.matcher(text.toLowerCase());
        boolean b1 = matcher.find();
        if (b1 ) System.out.println("Match!");
        return b1;
    }

}
